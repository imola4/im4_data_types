#![deny(
    missing_docs,
    missing_debug_implementations,
    missing_copy_implementations,
    trivial_casts,
    trivial_numeric_casts,
    unsafe_code,
    unstable_features,
    unused_import_braces,
    unused_qualifications
)]
//! Crate per la gestione dei dati contenuti nel database del gruppo Imola 4
extern crate bcrypt;
#[macro_use]
extern crate serde_derive;
extern crate serde;

/// Utilità
pub mod utilities {
    /// Modulo per la gestione degli ID (alpha) TODO
    pub mod id {
        /// alpha
        #[derive(Debug)]
        pub struct UUID {
            /// alpha
            pub id: String,
        }
    }
    /// Modulo per gestire data e ora
    pub mod dataeora{

        /// Rappresenta vari formati di data
        #[derive(Debug, Copy, Clone)]
        pub enum TipoData {
            /// Data nel formato GG/MM/AA (DD/MM/YY)
            GGMMAA,
            /// Data nel formato GG/MM/AAAA (DD/MM/YYYY)
            GGMMAAAAA,
            /// Data nel formato GG/MM/AAAA HH:MM:SS (DD/MM/YYYY HH:MM:SS)
            GGMMAAAAAHHMMSS,
        }

        /// Ritorna il timestamp come stringa in base al TipoData fornito
        pub fn timestamp_as_string(timestamp: i64, formato: TipoData) -> String{
            use chrono::offset::TimeZone;
            let formato_str: &str;
            match formato{
                TipoData::GGMMAA => formato_str = "%d/%m/%y",
                TipoData::GGMMAAAAA => formato_str = "%d/%m/%Y",
                TipoData::GGMMAAAAAHHMMSS => formato_str = "%d/%m/%Y %T"

            }
            return chrono::prelude::Local.timestamp(timestamp, 0).format(formato_str).to_string();
        }

        /// Questa funzione ritorna il timestamp UNIX come intero a 64 bit
        pub fn timestamp() -> i64 {
            let start = std::time::SystemTime::now();
            start
                .duration_since(std::time::UNIX_EPOCH)
                .unwrap()
                .as_secs() as i64
        }
    }
}

/// Modulo che contiene i componenti necessari alla rappresentazione di un utente
pub mod utente {
    #[derive(Serialize, Deserialize, Debug, Copy, Clone)]
    /// Enum per codificare i sessi
    pub enum Sesso {
        /// Maschio
        M,
        /// Femmina
        F,
    }

    #[derive(Serialize, Deserialize, Debug)]
    /// Struct che contiene i dati dell'utente. Corrisponde grossomodo alla struttura del record nel DB. Fornisce inoltre metodi di aiuto
    pub struct Utente {
        /// ID univoco dell'utente (DB Im4)
        pub id: String,
        /// Nome utente
        pub nome_utente: String,
        /// Hash BCrypt della password dell'utente
        pub password: String,
        /// Timestamp di scadenza della password
        pub scadenza_password: i64,
        /// Nome dell'utente
        pub nome: String,
        /// Cognome dell'utente
        pub cognome: String,
        /// Percorso della foto su assets.imola4.tk
        pub foto: String,
        /// Timestamp della data di nascita
        pub data_nascita: i64,
        /// Luogo di nascita
        pub luogo_nascita: String,
        /// Codice censimento AGESCI
        pub id_agesci: String,
        /// Codice fiscale
        pub codice_fiscale: String,
        /// Sesso
        pub sesso: Sesso,
        /// Timestamp della data di registrazione
        pub data_aggiunta: i64,
    }

    impl Utente {
        /// Ritorna il nome completo dell'utente (prima il nome e poi il cognome)
        pub fn nome_completo(&self) -> String {
            return self.nome.clone() + " " + &self.cognome;
        }
        /// Controlla la password fornita come parametro con l'hash memorizzato nella struct Utente
        pub fn controlla_password(&self, password: String) -> Result<bool, &str> {
            if self.scadenza_password < super::utilities::dataeora::timestamp() {
                return Err("Password scaduta");
            }
            Ok(bcrypt::verify(password.as_str(), self.password.as_str()).unwrap())
        }
    }
}
